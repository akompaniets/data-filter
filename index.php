<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 15.10.14
 * Time: 11:10
 */
define('BASEPATH', __DIR__);
$corePath = BASEPATH . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'Base.php';
$autoLoaderPath = BASEPATH . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'SplClassLoader.php';
require_once($corePath);
require_once($autoLoaderPath);
if (PHP_SAPI !== 'cli') {
	\core\Base::getInstance(new \core\SplClassLoader())->start();
	(new core\Router())->start();
}
