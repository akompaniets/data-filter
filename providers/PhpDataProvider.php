<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 16.10.14
 * Time: 17:29
 */

namespace providers;


class PhpDataProvider extends Provider {

	/**
	 * @throws \Exception
	 * @return array
	 */
	protected function getFileContent() {
		$this->rawData = include($this->filePath);
	}

	protected function parseData() {
		if (!is_array($this->rawData)) {
			throw new \Exception('Invalid data provided in file ' . $this->filePath);
		}
		$result = [];
		foreach($this->rawData as $locale => $currencies) {
			foreach($currencies as $currency => $info) {
				$result[] = [
					self::LOCALE_INDEX => $locale,
					self::CODE_INDEX => $currency,
					self::VALUE_INDEX => $info['value'],
					self::NAME_INDEX => $info['name']
				];
			}
		}
		$this->parsedData = $result;
	}
}