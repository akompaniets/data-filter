<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 16.10.14
 * Time: 17:39
 */

namespace providers;



class ProviderFactory {


	/**
	 * @param $filePath
	 * @throws \Exception
     * @return Provider
	 *
	 */
	static function getProvider($filePath) {
        $type = Provider::resolveProviderTypeByPath($filePath);
		switch($type) {
			case Provider::PHP: return new PhpDataProvider($filePath);break;
            case Provider::JSON: return new JsonDataProvider($filePath);break;
            case Provider::XML: return new XmlDataProvider($filePath);break;
			default: throw new \Exception('Unknown data provider type');
		}
	}
} 