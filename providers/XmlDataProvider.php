<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 17.10.14
 * Time: 10:37
 */
namespace providers;
class XmlDataProvider extends Provider {

	protected function  parseData() {
		$result = [];
		try {
			$xml = simplexml_load_string($this->rawData);
		} catch (\ErrorException $e) {
			throw new \Exception('Corrupted data provided in ' . $this->filePath);
		}
		foreach($xml as $item) {
			$result[] = [
				'locale' => (string)$item->attributes()['Type'],
				'code' => (string)$item->Code,
				'value' => floatval($item->Value),
				'name' => (string)$item->Description
			];
		}
		$this->parsedData = $result;
	}
}