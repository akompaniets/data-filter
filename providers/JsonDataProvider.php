<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 17.10.14
 * Time: 17:29
 */

namespace providers;


class JsonDataProvider extends Provider {

	const CODE_INDEX = 0;
	const NAME_INDEX = 1;
	const VALUE_INDEX = 2;
	const LOCALE_INDEX = 3;

	protected function  parseData() {
		$result = [];
		$dataArray = \json_decode($this->rawData, true);
		if (!is_array($dataArray)) {
			throw new \Exception('Corrupted data provided in file ' . $this->filePath);
		}
		foreach($dataArray as $item) {
			$result[] = [
				'locale' => $item[self::LOCALE_INDEX],
				'code' => $item[self::CODE_INDEX],
				'value' => floatval(str_replace(',', '.', $item[self::VALUE_INDEX])),
				'name' => $item[self::NAME_INDEX]
			];
		}
		$this->parsedData = $result;
	}
}