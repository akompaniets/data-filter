<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 16.10.14
 * Time: 17:26
 */

namespace providers;


use SebastianBergmann\Exporter\Exception;

abstract class Provider implements filters\Filter {

	protected $filePath;
	protected $rawData;
	protected $parsedData;

	const LOCALE_INDEX = 'locale';
	const CODE_INDEX = 'code';
	const VALUE_INDEX = 'value';
	const NAME_INDEX = 'name';

    const PHP = 3;
    const JSON = 1;
    const XML = 2;

    private static $extensionResolveArray = [
        'php' => self::PHP,
        'json' => self::JSON,
        'xml' => self::XML
    ];

	function __construct($filePath) {
		$this->setFilePath($filePath);
	}

	function setFilePath($filePath) {
		if ($filePath && is_file($filePath)) {
			$this->filePath = $filePath;
		} else {
			throw new Exception("Unknown path or path not set " . $filePath);
		}
	}

	/**
	 * @return mixed
	 */
	protected function getFileContent() {
		$this->rawData = file_get_contents($this->filePath);
	}


	function getParsedData() {
		if (is_null($this->parsedData)) {
			$this->getFileContent();
			$this->parseData();
		}
		return $this->parsedData;
	}

	function applyFilter() {
		return $this->getParsedData();
	}

    static function resolveProviderTypeByPath($path) {
        $ext = strtolower(pathinfo($path, PATHINFO_EXTENSION));
        if (isset(self::$extensionResolveArray[strtolower($ext)])) {
            return self::$extensionResolveArray[$ext];
        }
        return false;
    }

    function getColumn($columnName) {
        $data = array_unique(array_map(function($item) use ($columnName) {
            return $item[$columnName];
        }, $this->getParsedData()));
        return array_combine($data, $data);
    }

	abstract protected function  parseData();
} 