<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 17.10.14
 * Time: 17:55
 */
namespace providers\filters;
interface Filter {
	/**
	 * @return array
	 */
	function applyFilter();
} 