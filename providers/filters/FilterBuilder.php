<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 19.10.14
 * Time: 16:21
 */

namespace providers\filters;


use providers\Provider;

class FilterBuilder {

    private $dataProvider;
    private $filters = [];

    static function getBuilder(Provider $dataProvider) {
        return new self($dataProvider);
    }

    function __construct(Provider $dataProvider) {
        $this->dataProvider = $dataProvider;
    }

    function addFilter($attribute, $filterType, $params) {
        if ($params['value'] === '') { return $this;}
        $filterClassName = '\\providers\\filters\\' . ucfirst($filterType) . 'Filter';
        if (class_exists($filterClassName)) {
            $params['fieldName'] = $attribute;
            $this->filters[$attribute][$filterType] = new $filterClassName($params);
        } else {
            throw new \Exception('Unknown filter ' . $filterType. '. Make sure that class ' . $filterClassName . ' exists in filters folder');
        }
        return $this;
    }

    function addFilters($filters) {
        foreach($filters as $attributeName => $filtersByField) {
            foreach($filtersByField as $filterType => $params) {
                $this->addFilter($attributeName, $filterType, $params);
            }
        }
        return $this;
    }

    /**
     * @return Filter
     */
    function decorateFilters() {
        $tmp = [];
        foreach($this->filters as $attribute => $filters) {
            foreach($filters as $filter) {
                $tmp[] = $filter;
            }
        }
        foreach($tmp as $k => $filter) {
            if (isset($tmp[$k + 1])) {
                $filter->setFilter($tmp[$k + 1]);
            }
        }
        if ($tmp) {
            $lastFilter = end($tmp);
            $lastFilter->setFilter($this->dataProvider);
        } else {
            $tmp[] = $this->dataProvider;
        }
        return reset($tmp);
    }

    function getFilters() {
        return $this->filters;
    }

    function getDataProvider() {
        return $this->dataProvider;
    }

    function renderFilterInput($attribute, $filterType, $selectData = [], $options = []) {
        $value = null;
        if (isset($this->filters[$attribute][$filterType])) {
            $value = $this->filters[$attribute][$filterType]->getValue();
        }
        $res = '';
        if (!empty($options['filterTypes'])) {
            $currentAdditionalFilterType = null;
            if (isset($this->filters[$attribute][$filterType])) {
                $currentAdditionalFilterType = $this->filters[$attribute][$filterType]->getAdditionalFilterType();
            }
            $filterClass = '\\providers\\filters\\' . ucfirst($filterType) . 'Filter';
            $res .= $filterClass::getAdditionalFilterTypeSelect($attribute, $filterType, $options['filterTypes'], $currentAdditionalFilterType);
        }
        if ($selectData) {
            $options['empty'] = isset($options['empty']) ? $options['empty'] : '';
            $res .= '<select name="filter[' . $attribute . '][' . $filterType . '][value]">';
            if ($options['empty'] !== null) {
                $res .= '<option value="' . $options['empty'] . '">' . $options['empty'] . '</option>';
            }
            foreach($selectData as $optionValue => $option) {
                $res .= '<option value="' . $optionValue . '" ' . ($value !== null && $value == $optionValue ? 'selected' : '') .'>' . $option . '</option>';
            }
            $res .= '</select>';
        } else {
            $res .= '<input type="text" value="' . $value . '" name="filter[' . $attribute .  '][' . $filterType . '][value]"/>';
        }
        return $res;
    }
} 