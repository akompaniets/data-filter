<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 17.10.14
 * Time: 17:58
 */

namespace providers\filters;
abstract class AbstractFilter implements Filter {
	/**
	 * @var Filter
	 */
	protected $filter;
    protected  $fieldName;
    protected  $value;
    protected $additionalFilterType;

    function __construct($params, Filter $filter = null) {
        if ($filter) {
            $this->filter = $filter;
        }
        $this->fieldName = $params['fieldName'];
        $this->value = $params['value'];
        $this->additionalFilterType = isset($params['additionalFilterType']) ? $params['additionalFilterType'] : $this->defaultAdditionalFilterType();
    }


//    function applyFilter() {
//        if (!$this->filter) {
//            throw new \Exception('Unable to filtrate without data passed');
//        }
//    }

    function setFilter(Filter $filter) {
        $this->filter = $filter;
    }

    function getFilter() {
        return $this->filter;
    }

    function getValue() {
        return $this->value;
    }

    function setValue($value) {
        $this->value = $value;
    }

    function defaultAdditionalFilterType() {
        throw new \Exception('Method not implemented');
    }

    static function getAdditionalFilterTypeSelect($attribute, $type, $additionalFilterTypes, $currentAdditionalType = null) {
        $select = '<select class="additional_filter" name="filter[' . $attribute . '][' . $type . '][additionalFilterType]">';
        foreach($additionalFilterTypes as $additionalFilterType) {
            if (isset(static::getAdditionalTypeLabels()[$additionalFilterType])) {
                $select .= '<option ' . ($currentAdditionalType == $additionalFilterType ? 'selected' : '') . ' value="' . $additionalFilterType . '">' . static::getAdditionalTypeLabels()[$additionalFilterType] . '</option>';
            }
        }
        $select .= '</select>';
        return $select;
    }

    function getAdditionalFilterType() {
        return $this->additionalFilterType;
    }

    function setAdditionalFilterType($type) {
        $this->additionalFilterType = $type;
    }

    static function getAdditionalTypeLabels() {
        throw new \Exception('Method not implemented');
    }
}