<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 20.10.14
 * Time: 13:37
 */

namespace providers\filters;


class SortFilter extends AbstractFilter {
    /**
     * @return array
     */

    const ASC = 2;
    const DESC = 1;

    /**
     * @return array
     * @throws \Exception
     */
    function applyFilter() {
        if (!$this->filter) {
            throw new \Exception('Unable to filtrate wihout data passed');
        }
        $data = $this->filter->applyFilter();
        switch($this->value) {
            case self::ASC:
                usort($data, function($a, $b) {
                    return strcasecmp($a[$this->fieldName], $b[$this->fieldName]);
                });
                return $data;
            case self::DESC:
                usort($data, function($a, $b) {
                    return -strcmp($a[$this->fieldName], $b[$this->fieldName]);
                });
                return $data;
            default: throw new \Exception('Unknown type ' . $this->additionalFilterType);
        }
    }

    function defaultAdditionalFilterType() {
        return self::ASC;
    }

    static function getAdditionalTypeLabels() {
        return [
            self::DESC => 'descending',
            self::ASC => 'ascending',
        ];
    }
} 