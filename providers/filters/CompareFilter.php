<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 17.10.14
 * Time: 17:56
 */
namespace providers\filters;
class CompareFilter extends AbstractFilter {

	const LIKE = 1;
	const LESS_THEN = 2;
	const MORE_THEN = 3;
	const EQUALS = 4;
    const LESS_OR_EQUALS = 5;
    const MORE_OR_EQUALS = 6;


	/**
	 * @return array
	 * @throws \Exception
	 */
	function applyFilter() {
		$res = [];
        if (!$this->filter) {
            throw new \Exception('Unable to filtrate wihout data passed');
        }
		foreach($this->filter->applyFilter() as $item) {
			switch($this->additionalFilterType) {
				case self::LIKE:
					if (isset($item[$this->fieldName]) && strpos(mb_strtolower($item[$this->fieldName]), mb_strtolower($this->value)) !== false) {
						$res[] = $item;
					}
					break;
				case self::LESS_THEN:
					if (!is_numeric($this->value)) {
						throw new \Exception('Less filter expects numeric value, given ' . gettype($this->value));
					}
					if (isset($item[$this->fieldName]) && is_numeric($item[$this->fieldName]) && $item[$this->fieldName] < $this->value) {
						$res[] = $item;
					}
					break;
                case self::MORE_THEN:
                    if (!is_numeric($this->value)) {
                        throw new \Exception('Less filter expects numeric value , given ' . gettype($this->value));
                    }
                    if (isset($item[$this->fieldName]) && is_numeric($item[$this->fieldName]) && $item[$this->fieldName] > $this->value) {
                        $res[] = $item;
                    }
                    break;
                case self::EQUALS:
                    if (isset($item[$this->fieldName]) && strtolower($item[$this->fieldName]) == strtolower($this->value)) {
                        $res[] = $item;
                    }
                    break;
                case self::MORE_OR_EQUALS:
                    if (!is_numeric($this->value)) {
                        throw new \Exception('Less filter expects numeric value , given ' . gettype($this->value));
                    }
                    if (isset($item[$this->fieldName]) && is_numeric($item[$this->fieldName]) && $item[$this->fieldName] >= $this->value) {
                        $res[] = $item;
                    }
                    break;
                case self::LESS_OR_EQUALS:
                    if (!is_numeric($this->value)) {
                        throw new \Exception('Less filter expects numeric value , given ' . gettype($this->value));
                    }
                    if (isset($item[$this->fieldName]) && is_numeric($item[$this->fieldName]) && $item[$this->fieldName] <= $this->value) {
                        $res[] = $item;
                    }
                    break;
				default: throw new \Exception('Unknown type ' . $this->additionalFilterType);
			}

		}
		return $res;
	}


    function defaultAdditionalFilterType() {
        return self::LIKE;
    }

    static function getAdditionalTypeLabels() {
        return [
            self::LIKE => 'like',
            self::LESS_THEN => '<',
            self::MORE_THEN => '>',
            self::EQUALS => '=',
            self::MORE_OR_EQUALS => '>=',
            self::LESS_OR_EQUALS => '<='
        ];
    }


}