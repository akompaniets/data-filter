<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 15.10.14
 * Time: 11:40
 */

namespace core;


class Router {

	public $controllerName = 'main';
	public $actionName = 'index';
	private $controller;

	function __construct(Controller $controller = null) {
		if ($controller) {
			$this->controller = $controller;
		} else {
			$controllerName = $this->getControllerName();
			if (class_exists($controllerName)) {
				$this->controller = new $controllerName;
			} else {
				throw new HttpException(404, 'Not found');
			}
		}
	}

	function getController() {
		return $this->controller;
	}

	function start() {
		$action = $this->getActionName();
		if (method_exists($this->controller, $action)) {
			$this->controller->$action();
		} else {
			throw new HttpException(404, 'Not found');
		}
	}

	private function getControllerName() {
		$routes = explode('/', $_SERVER['REQUEST_URI']);
		if (!empty($routes[1])) {
			$this->controllerName = $routes[1];
		}
		$controllerName = ucfirst($this->controllerName) . 'Controller';
		return '\\controllers\\' . $controllerName;
	}

	private function getActionName() {
		$routes = explode('/', $_SERVER['REQUEST_URI']);
		if (!empty($routes[2])) {
			$this->actionName = $routes[2];
		}
		return 'action' . ucfirst($this->actionName);
	}
}