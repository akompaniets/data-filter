<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 15.10.14
 * Time: 11:42
 */

namespace core;


abstract class Controller {

    private function getViewPath($viewFile) {
        $controllerName = mb_substr(get_called_class(), strripos(get_called_class(), '\\') + 1);
        $viewsDirName = str_replace('Controller', '', $controllerName);
        return BASEPATH . '/views/' . strtolower($viewsDirName) . '/' . $viewFile . '.php';
    }

    function render($viewFile, $data = []) {
        $viewPath = $this->getViewPath($viewFile);
        if (is_file($viewPath)) {
            extract($data, EXTR_PREFIX_SAME, 'data');
            require($viewPath);
        } else {
            throw new \Exception('Unable to render view file ' . $viewFile);
        }
    }
} 