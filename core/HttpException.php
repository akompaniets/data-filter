<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 16.10.14
 * Time: 17:16
 */

namespace core;

class HttpException extends \Exception {
	public $statusCode;

	public function __construct($status, $message = null, $code = 0) {
		$this->statusCode = $status;
		parent::__construct($message, $code);
	}
}