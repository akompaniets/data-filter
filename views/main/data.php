<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<style>
    table, th, td {
        border: 1px solid black;
    }
    table {
        border-collapse: collapse;
    }
</style>
<?php
use \providers\Provider;
use \providers\filters\CompareFilter;
use \providers\filters\SortFilter;?>
<h2>Data type: <?= $dataType ?></h2>
<a href="/main">Back to sources list</a> |
<a href="">Reset filter</a> |
<a href="/tests/_output/coverage/index.html">Test coverage results</a>
<form method="post" id="filter-form">
<table>
    <thead>
        <tr>
            <th>Locale</th>
            <th>Code</th>
            <th>Value</th>
            <th>Name</th>
        </tr>
    </thead>
    <tr>
        <td>
            <?= $builder->renderFilterInput(Provider::LOCALE_INDEX, 'sort', SortFilter::getAdditionalTypeLabels())?>
        </td>
        <td>
            <?= $builder->renderFilterInput(Provider::CODE_INDEX, 'sort', SortFilter::getAdditionalTypeLabels())?>
        </td>
        <td>
            <?= $builder->renderFilterInput(Provider::VALUE_INDEX, 'sort', SortFilter::getAdditionalTypeLabels())?>
        </td>
        <td>
            <?= $builder->renderFilterInput(Provider::NAME_INDEX, 'sort', SortFilter::getAdditionalTypeLabels())?>
        </td>
    </tr>
    <tr>
        <td>
            <?= $builder->renderFilterInput(Provider::LOCALE_INDEX, 'compare', $locales)?>
        </td>
        <td>
            <?= $builder->renderFilterInput(Provider::CODE_INDEX, 'compare')?>
        </td>
        <td>
            <?= $builder->renderFilterInput(Provider::VALUE_INDEX, 'compare', [], ['filterTypes' => [
                CompareFilter::EQUALS, CompareFilter::LESS_THEN, CompareFilter::MORE_THEN, CompareFilter::MORE_OR_EQUALS, CompareFilter::LESS_OR_EQUALS
            ]])?>
        </td>
        <td>
            <?= $builder->renderFilterInput(Provider::NAME_INDEX, 'compare', [], ['filterTypes' => [
                CompareFilter::LIKE, CompareFilter::EQUALS,
            ]])?>
        </td>
    </tr>
    <?php
    foreach($result as $item) {
        echo '<tr>';
        echo '<td>' . $item[Provider::LOCALE_INDEX] . '</td>';
        echo '<td>' . $item[Provider::CODE_INDEX] . '</td>';
        echo '<td>' . $item[Provider::VALUE_INDEX] . '</td>';
        echo '<td>' . $item[Provider::NAME_INDEX] . '</td>';
        echo '<tr/>';
    }
    ?>
</table>
</form>
<script>
    $(document).ready(function(){
        $('input, select:not(\'.additional_filter\')').change(function(){
            $(this).closest('form').submit();
        })
    });
</script>