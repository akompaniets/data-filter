# README #

This README would normally document whatever steps are necessary to get your application up and running.

[available on heroku](http://fathomless-peak-7538.herokuapp.com)

### Filter application allows to filter data from different sources in easy way###

* less, more, equals, like filters
* sorting
* built with autoloading, router, mvc, decorator, template method, factory patterns and easily extensible

### How do I get set up? ###

* Setup is extremely easy via Vagrant and composer 
* git clone https://akompaniets@bitbucket.org/akompaniets/data-filter.git . && vagrant up && composer install
* Almost fully tested via Codeception and Mockery