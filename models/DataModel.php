<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 15.10.14
 * Time: 11:23
 */
namespace models;

use providers\filters\FilterBuilder;
use \providers\ProviderFactory;

class DataModel extends \core\Model {

    /**
     * @param $path
     * @param null $filters
     * @return FilterBuilder
     * @throws \Exception
     */
	function getData($path, $filters = null) {
        $builder = $this->getBuilder($path);
        if ($filters) {
            $builder->addFilters($filters);
        }
		return $builder;
	}

    function getBuilder($path) {
        $provider = ProviderFactory::getProvider($path);
        return new FilterBuilder($provider);
    }


} 