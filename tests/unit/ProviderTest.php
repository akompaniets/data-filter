<?php

use \providers\Provider;
class ProviderTest extends \Codeception\TestCase\Test
{
   /**
    * @var \UnitTester
    */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
        \Mockery::close();
    }



    function testApplyFilter() {
        $mock = \Mockery::mock('\\providers\\PhpDataProvider[getParsedData]', [BASEPATH . '/tests/unit/fixtures/data.php']);
        $mock->shouldReceive('getParsedData')->once();
        $mock->applyFilter();
    }


    function testResolveProviderTypeByPath() {
        foreach(self::resolveProvider() as $item) {
            $this->assertEquals($item[1], Provider::resolveProviderTypeByPath($item[0]));
        }

    }

    static function resolveProvider() {
        return [
            ['test.php', Provider::PHP],
            ['dir/test.json', Provider::JSON],
            ['dir/test.XmL', Provider::XML],
            ['dir/test.xls', false]
        ];
    }

    function testGetColumn() {
        $data = [
            ['code' => 'USD', 'name' => 'dollar', 'locale' => 'eu'],
            ['code' => 'UAH', 'name' => 'hryvna', 'locale' => 'eu'],
            ['code' => 'JPN', 'name' => 'yena', 'locale' => 'world']
        ];
        $mock = \Mockery::mock('\\providers\\PhpDataProvider[getParsedData]', [BASEPATH . '/tests/unit/fixtures/data.php']);
        $mock->shouldReceive('getParsedData')->andReturn($data);

        $this->assertEquals(['eu' => 'eu', 'world' => 'world'], $mock->getColumn('locale'));
        $this->assertEquals(['USD' => 'USD', 'UAH' => 'UAH', 'JPN' => 'JPN'], $mock->getColumn('code'));
    }



}