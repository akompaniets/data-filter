<?php
return array(
	'europe' => array(
		"PLN" => array(
			"name" => "zloty polski",
			"value" => 1.0,
		)
	),
	'world' => array(
		"USD" => array(
			"name" => "dolar amerykanski",
			"value" => 3.33,
		),
	),
);