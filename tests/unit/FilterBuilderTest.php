<?php

use \providers\filters\FilterBuilder;
use \providers\filters\CompareFilter;
class FilterBuilderTest extends \Codeception\TestCase\Test
{
   /**
    * @var \UnitTester
    */
    protected $tester;
    private $dataProvider;

    protected function _before()
    {
        $this->dataProvider = \Mockery::mock('\\providers\\Provider');
    }

    protected function _after()
    {
    }

    // tests
    public function testInitializing() {
        $builder = new FilterBuilder($this->dataProvider);
        $this->assertEquals($this->dataProvider, $builder->getDataProvider());
    }

    function testAddFilters() {
        $filtersArray = [
            'code' => [
                'sort' => [
                    'value' => 1
                ]
            ]
        ];
        $builder = Mockery::mock('\\providers\\filters\\FilterBuilder[addFilter]', [$this->dataProvider]);
        $builder->shouldReceive('addFilter')->with('code', 'sort', $filtersArray['code']['sort'])->once();
        $builder->addFilters($filtersArray);
    }

    function testAddFilter() {
        $builder = new FilterBuilder($this->dataProvider);
        $this->assertEmpty($builder->getFilters());
        $builder->addFilter('code', 'sort', ['value' => 1]);
        $this->assertEquals(1, count($builder->getFilters()));
        $this->assertEquals('providers\\filters\\SortFilter', get_class($builder->getFilters()['code']['sort']));
    }

    function testThrowsExceptionOnUndefinedFilter() {
        $builder = new FilterBuilder($this->dataProvider);
        try {
            $builder->addFilter('code', 'sozxcvrt', ['value' => 1]);
            $this->fail('Unknown filter exception should be thrown');
        } catch (Exception $e) {

        }
    }

    function testDecorateFilters() {
        $filtersArray = [
            'code' => [
                'sort' => [
                    'value' => 1
                ]
            ],
            'name' => [
                'compare' => [
                    'value' => 'abc'
                ]
            ]
        ];
        $builder = new FilterBuilder($this->dataProvider);
        $builder->addFilters($filtersArray);
        $filters = $builder->getFilters();
        $decorated = $builder->decorateFilters();
        $this->assertEquals($filters['code']['sort'], $decorated);
        $this->assertEquals($filters['name']['compare'], $decorated->getFilter());
        $this->assertEquals($filters['name']['compare']->getFilter(), $this->dataProvider);
    }

    function testDecorateWithoutFilters() {
        $filtersArray = [];
        $builder = new FilterBuilder($this->dataProvider);
        $builder->addFilters($filtersArray);
        $decorated = $builder->decorateFilters();
        $this->assertEquals($this->dataProvider, $decorated);
    }

    function testRenderFilterInputText() {
        $builder = new FilterBuilder($this->dataProvider);
        $result = $builder->renderFilterInput('name', 'compare');
        $this->assertEquals('<input type="text" value="" name="filter[name][compare][value]"/>', $result);
    }

    function testRenderFilterInputTextWithValue() {
        $filtersArray = [
            'name' => [
                'compare' => [
                    'value' => 'abc'
                ]
            ]
        ];
        $builder = new FilterBuilder($this->dataProvider);
        $builder->addFilters($filtersArray);
        $result = $builder->renderFilterInput('name', 'compare');
        $this->assertEquals('<input type="text" value="abc" name="filter[name][compare][value]"/>', $result);
    }

    function testRenderAdditionalFilterTypesSelect() {
//        $filtersArray = [
//            'name' => [
//                'compare' => [
//                    'value' => 'abc'
//                ]
//            ]
//        ];
        $builder = new FilterBuilder($this->dataProvider);
//        $builder->addFilters($filtersArray);
        $result = $builder->renderFilterInput('name', 'compare', [], ['filterTypes' => [CompareFilter::LESS_THEN]]);
        $this->assertEquals('<select class="additional_filter" name="filter[name][compare][additionalFilterType]"><option  value="2"><</option></select><input type="text" value="" name="filter[name][compare][value]"/>', $result);
    }

    function testRenderSelectedAdditionalFilterType() {
        $filtersArray = [
            'name' => [
                'compare' => [
                    'value' => 'abc',
                    'additionalFilterType' => CompareFilter::MORE_THEN
                ]
            ]
        ];
        $builder = new FilterBuilder($this->dataProvider);
        $builder->addFilters($filtersArray);
        $result = $builder->renderFilterInput('name', 'compare', [], ['filterTypes' => [CompareFilter::LESS_THEN, CompareFilter::MORE_THEN]]);
        $this->assertEquals('<select class="additional_filter" name="filter[name][compare][additionalFilterType]"><option  value="2"><</option><option selected value="3">></option></select><input type="text" value="abc" name="filter[name][compare][value]"/>', $result);
    }

    function testRenderListDataFilter() {
        $builder = new FilterBuilder($this->dataProvider);
        $builder->addFilters([]);
        $result = $builder->renderFilterInput('name', 'compare', ['usd' => 'USD', 'eur' => 'EUR']);
        $this->assertEquals('<select name="filter[name][compare][value]"><option value=""></option><option value="usd" >USD</option><option value="eur" >EUR</option></select>', $result);
    }

    function testRenderSelectedListDataFilter() {
        $filtersArray = [
            'name' => [
                'compare' => [
                    'value' => 'usd',
                ]
            ]
        ];
        $builder = new FilterBuilder($this->dataProvider);
        $builder->addFilters($filtersArray);
        $result = $builder->renderFilterInput('name', 'compare', ['usd' => 'USD', 'eur' => 'EUR']);
        $this->assertEquals('<select name="filter[name][compare][value]"><option value=""></option><option value="usd" selected>USD</option><option value="eur" >EUR</option></select>', $result);
    }
}