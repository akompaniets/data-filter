<?php
use \providers\filters\SortFilter;

class SortFilterTest extends \Codeception\TestCase\Test
{
   /**
    * @var \UnitTester
    */
    private $dataProvider;

    static $returnData = [
        ['code' => 'USD', 'locale' => 'world', 'value' => 1],
        ['code' => 'EUR', 'locale' => 'europe', 'value' => 2]
    ];

    protected function _before()
    {
        $dataProviderMock = Mockery::mock('\\providers\\Provider');
        $dataProviderMock->shouldReceive('applyFilter')->andReturn(self::$returnData);
        $this->dataProvider = $dataProviderMock;
    }

    protected function _after()
    {
    }

    function testDescSortFilter() {
        $compareFilter = new SortFilter(['fieldName' => 'code', 'value' => SortFilter::ASC], $this->dataProvider);
        $this->assertEquals([
            ['code' => 'EUR', 'locale' => 'europe', 'value' => 2],
            ['code' => 'USD', 'locale' => 'world', 'value' => 1],
        ], $compareFilter->applyFilter());
    }

    function testAscSortFilter() {
        $compareFilter = new SortFilter(['fieldName' => 'code', 'value' => SortFilter::DESC], $this->dataProvider);
        $this->assertEquals([
            ['code' => 'USD', 'locale' => 'world', 'value' => 1],
            ['code' => 'EUR', 'locale' => 'europe', 'value' => 2]
        ], $compareFilter->applyFilter());
    }

    function testThrowsUnknownTypeException() {
        $compareFilter = new SortFilter(
            ['fieldName' => 'value', 'value' => 'abcdef'],
            $this->dataProvider
        );
        try {
            $compareFilter->applyFilter();
            $this->fail('Unknown additional filter type expected');
        } catch (Exception $e) {
//            var_dump($e);exit;
        }
    }

    function testThrowsExceptionOnApplyFilterIfNotSet() {
        $compareFilter = new SortFilter(
            ['fieldName' => 'value', 'value' => SortFilter::DESC]
        );
        try {
            $compareFilter->applyFilter();
            $this->fail('Exception should be thrown');
        } catch (Exception $e) {}
    }

}