<?php

use \providers\filters\CompareFilter;
class CompareFilterTest extends \Codeception\TestCase\Test
{
   /**
    * @var \UnitTester
    */
    protected $tester;


    private $dataProvider;

    static $returnData = [
        ['code' => 'USD', 'locale' => 'world', 'value' => 1],
        ['code' => 'EUR', 'locale' => 'europe', 'value' => 2]
    ];

    protected function _before()
    {
        $dataProviderMock = Mockery::mock('\\providers\\Provider');
        $dataProviderMock->shouldReceive('applyFilter')->andReturn(self::$returnData);
        $this->dataProvider = $dataProviderMock;
    }

    protected function _after()
    {
    }

    function testDefaultAdditionalTypeSets() {
        $compareFilter = new CompareFilter(['fieldName' => 'code', 'value' => 'USD'], $this->dataProvider);
        $this->assertEquals(CompareFilter::LIKE, $compareFilter->defaultAdditionalFilterType());
    }

    function testLikeFilter() {
        $compareFilter = new CompareFilter(['fieldName' => 'code', 'value' => 'us'], $this->dataProvider);
        $this->assertEquals([
            ['code' => 'USD', 'locale' => 'world', 'value' => 1]
        ], $compareFilter->applyFilter());
    }

    function testLessFilter() {
        $compareFilter = new CompareFilter(
            ['fieldName' => 'value', 'value' => 2, 'additionalFilterType' => CompareFilter::LESS_THEN],
            $this->dataProvider
        );
        $this->assertEquals([
            ['code' => 'USD', 'locale' => 'world', 'value' => 1],
        ], $compareFilter->applyFilter());
    }

    function testMoreFilter() {
        $compareFilter = new CompareFilter(
            ['fieldName' => 'value', 'value' => 1, 'additionalFilterType' => CompareFilter::MORE_THEN],
            $this->dataProvider
        );
        $this->assertEquals([
            ['code' => 'EUR', 'locale' => 'europe', 'value' => 2]
        ], $compareFilter->applyFilter());
    }

    function testEqualsFilter() {
        $compareFilter = new CompareFilter(
            ['fieldName' => 'code', 'value' => 'usd', 'additionalFilterType' => CompareFilter::EQUALS],
            $this->dataProvider
        );
        $this->assertEquals([
            ['code' => 'USD', 'locale' => 'world', 'value' => 1],
        ], $compareFilter->applyFilter());
        $compareFilter->setValue('us');
        $this->assertEmpty($compareFilter->applyFilter());
    }

    function testMoreOrEqualsFilter() {
        $compareFilter = new CompareFilter(
            ['fieldName' => 'value', 'value' => 1, 'additionalFilterType' => CompareFilter::MORE_OR_EQUALS],
            $this->dataProvider
        );
        $this->assertEquals([
            ['code' => 'USD', 'locale' => 'world', 'value' => 1],
            ['code' => 'EUR', 'locale' => 'europe', 'value' => 2]
        ], $compareFilter->applyFilter());
    }

    function testLessOrEqualsFilter() {
        $compareFilter = new CompareFilter(
            ['fieldName' => 'value', 'value' => 2, 'additionalFilterType' => CompareFilter::LESS_OR_EQUALS],
            $this->dataProvider
        );
        $this->assertEquals([
            ['code' => 'USD', 'locale' => 'world', 'value' => 1],
            ['code' => 'EUR', 'locale' => 'europe', 'value' => 2]
        ], $compareFilter->applyFilter());
    }

    function testThrowsExceptionOnApplyFilterIfNotSet() {
        $compareFilter = new CompareFilter(
            ['fieldName' => 'value', 'value' => 2, 'additionalFilterType' => CompareFilter::LESS_OR_EQUALS]
        );
        try {
            $compareFilter->applyFilter();
            $this->fail('Exception should be thrown');
        } catch (Exception $e) {}
    }

    function testThrowsExceptionOnNonNumericValue() {
        $compareFilter = new CompareFilter(
            ['fieldName' => 'value', 'value' => 'abcdef', 'additionalFilterType' => CompareFilter::LESS_THEN],
            $this->dataProvider
        );
        try {
            $compareFilter->applyFilter();
            $this->fail('Numerical value expected exception should be thrown');
        } catch (Exception $e) {
            $this->assertEquals('Less filter expects numeric value, given string', $e->getMessage());
        }
        $compareFilter->setAdditionalFilterType(CompareFilter::MORE_THEN);
        try {
            $compareFilter->applyFilter();
            $this->fail('Numerical value expected exception should be thrown');
        } catch (Exception $e) {}
        $compareFilter->setAdditionalFilterType(CompareFilter::LESS_OR_EQUALS);
        try {
            $compareFilter->applyFilter();
            $this->fail('Numerical value expected exception should be thrown');
        } catch (Exception $e) {}
        $compareFilter->setAdditionalFilterType(CompareFilter::MORE_OR_EQUALS);
        try {
            $compareFilter->applyFilter();
            $this->fail('Numerical value expected exception should be thrown');
        } catch (Exception $e) {}
    }

    function testThrowsUnknownAdditionalFilterTypeException() {
        $compareFilter = new CompareFilter(
            ['fieldName' => 'value', 'value' => 'abcdef', 'additionalFilterType' => 'random filter type'],
            $this->dataProvider
        );
        try {
            $compareFilter->applyFilter();
            $this->fail('Unknown additional filter type expected');
        } catch (Exception $e) {
        }
    }



}