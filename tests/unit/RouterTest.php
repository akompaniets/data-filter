<?php
use \Codeception\Util\Stub;
class RouterTest extends \PHPUnit_Framework_TestCase
{
    protected function setUp()
    {
    }

    protected function tearDown()
    {
    }

	public function testRouterWithParams() {
		$controller = Stub::make('\\core\\Controller');
		$router = new \core\Router($controller);
		$this->assertEquals($router->getController(), $controller);
	}

	/**
	 * @dataProvider getControllerProvider
	 */
	function testGetControllerName($uri, $result) {
		$_SERVER['REQUEST_URI'] = $uri;
		$router = Stub::make('\\core\\Router');
		$this->assertEquals($result, TestCommons::callPrivateMethod($router, 'getControllerName'));
	}

	public function getControllerProvider() {
		return [
			array('/', '\\controllers\\MainController'),
			array('/test', '\\controllers\\TestController'),
			array('/test/action', '\\controllers\\TestController'),
			array('/test/action/params', '\\controllers\\TestController'),
		];
	}

	/**
	 * @dataProvider getActionProvider
	 */
	function testGetActionName($uri, $result) {
		$_SERVER['REQUEST_URI'] = $uri;
		$router = Stub::make('\\core\\Router');
		$this->assertEquals($result, TestCommons::callPrivateMethod($router, 'getActionName'));
	}

	public function getActionProvider() {
		return [
			array('/', 'actionIndex'),
			array('/test', 'actionIndex'),
			array('/test/action', 'actionAction'),
			array('/test/action/params', 'actionAction'),
		];
	}

	function testConstruct() {
		$controller = Stub::make('\\core\\Controller');
		Stub::construct('\\core\\Router', [$controller], ['getControllerName' => Stub::never()]);


		$_SERVER['REQUEST_URI'] = '/';
		try {
			Stub::construct('\\core\\Router', [], ['getControllerName' => Stub::once()]);
		} catch (\core\HttpException $e) {
			return;
		}
		$this->fail('Exception should be thrown');
	}

	function testStart() {
		$_SERVER['REQUEST_URI'] = '/test/test';

		$controller = Stub::make('\\core\\Controller');
		$router = Stub::construct('\\core\\Router', [$controller], ['getActionName' => Stub::once()]);
		try {
			$router->start();
		} catch (\core\HttpException $e) {
			return;
		}
	}
}