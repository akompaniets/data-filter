<?php


class PhpDataProviderTest extends \Codeception\TestCase\Test
{
   /**
    * @var \UnitTester
    */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
	    \Mockery::close();
    }

    // tests
    public function testParsingData() {
		$phpDataProvider = new \providers\PhpDataProvider(BASEPATH . '/tests/unit/fixtures/data.php');
	    $data = $phpDataProvider->getParsedData();
	    $this->assertInternalType('array', $data);
	    $this->assertEquals(2, count($data));
	    $this->assertEquals([
	        array(
			    'code' => 'PLN',
			    'locale' => 'europe',
			    "name" => "zloty polski",
			    "value" => 1.0,
		    ),
		    array(
			    'code' => 'USD',
			    'locale' => 'world',
			    "name" => "dolar amerykanski",
			    "value" => 3.33,
		    )
	    ], $data);
	    $item = array_pop($data);
	    $this->assertArrayHasKey('locale', $item);
	    $this->assertArrayHasKey('name', $item);
	    $this->assertArrayHasKey('value', $item);
	    $this->assertArrayHasKey('code', $item);
    }

	function testExceptionWrongFilePath() {
		try {
			$phpDataProvider = new \providers\PhpDataProvider(BASEPATH . '/teszxcvzxcngfts/unit/fixtures/data.php');
			$this->fail('wrong file path exception should be thrown');
		} catch (Exception $e) {
			$this->assertEquals("Unknown path or path not set " . BASEPATH . '/teszxcvzxcngfts/unit/fixtures/data.php', $e->getMessage());
		}
	}

	function testExceptionInvalidData() {
		$phpDataProvider = new \providers\PhpDataProvider(BASEPATH . '/tests/unit/fixtures/data_wrong.php');
		try {
			$phpDataProvider->getParsedData();
			$this->fail('wrong data provided exception should be thrown');
		} catch (Exception $e) {
			$this->assertEquals('Invalid data provided in file ' . BASEPATH . '/tests/unit/fixtures/data_wrong.php', $e->getMessage());
		}
	}



}