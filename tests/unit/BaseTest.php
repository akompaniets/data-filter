<?php

use \Codeception\Util\Stub;
class BaseTest extends \Codeception\TestCase\Test
{
   /**
    * @var \UnitTester
    */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
	    \Mockery::close();
    }

    // tests
    public function testInitializing() {
	    try {
		    \core\Base::getInstance();
		    $this->fail('Exception must be thrown');
	    } catch (Exception $e) { }
	    $loader = \Mockery::mock('\\core\\SplClassLoader')->shouldReceive('register')->once()->getMock();
	    $base = \core\Base::getInstance($loader);
	    $base->start();
    }
}