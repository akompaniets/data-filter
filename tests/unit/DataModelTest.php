<?php

use \models\DataModel;
class DataModelTest extends \Codeception\TestCase\Test
{
   /**
    * @var \UnitTester
    */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
        Mockery::close();
    }

    // tests
    public function testGetData() {
        $filtersArray = [
            'name' => [
                'compare' => [
                    'value' => 'abc',
                ]
            ]
        ];
        $path = BASEPATH . '/tests/unit/fixtures/data.php';
        $builder = Mockery::mock('\\providers\\filters\\FilterBuilder');
        $builder->shouldReceive('addFilters')->with($filtersArray)->once();
        $model = Mockery::mock('\\models\\DataModel[getBuilder]');
        $model->shouldReceive('getBuilder')->with($path)->once()->andReturn($builder);
        $model->getData($path, $filtersArray);
    }

    function testGetBuilder() {
        $path = BASEPATH . '/tests/unit/fixtures/data.php';
        $model = new DataModel();
        $result = $model->getBuilder($path);
        $this->assertEquals('providers\\filters\\FilterBuilder', get_class($result));
    }

}