<?php


class XmlDataProviderTest extends \Codeception\TestCase\Test
{
   /**
    * @var \UnitTester
    */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

	public function testParsingData() {
		$phpDataProvider = new \providers\XmlDataProvider(BASEPATH . '/tests/unit/fixtures/data.xml');
		$data = $phpDataProvider->getParsedData();

		$this->assertInternalType('array', $data);
		$this->assertEquals(2, count($data));
		$this->assertEquals([
			array(
				'code' => 'PLN',
				'locale' => 'europe',
				"name" => "zloty polski",
				"value" => 1,
			),
			array(
				'code' => 'USD',
				'locale' => 'world',
				"name" => "dolar amerykanski",
				"value" => 3.33,
			)
		], $data);
		$item = array_pop($data);
		$this->assertArrayHasKey('locale', $item);
		$this->assertArrayHasKey('name', $item);
		$this->assertArrayHasKey('value', $item);
		$this->assertArrayHasKey('code', $item);
	}

	function testExceptionInvalidData() {
		$phpDataProvider = new \providers\XmlDataProvider(BASEPATH . '/tests/unit/fixtures/data_wrong.xml');
		try {
			$phpDataProvider->getParsedData();
			$this->fail('wrong data provided exception should be thrown');
		} catch (Exception $e) {
			$this->assertEquals('Corrupted data provided in ' . BASEPATH . '/tests/unit/fixtures/data_wrong.xml', $e->getMessage());
		}
	}
}