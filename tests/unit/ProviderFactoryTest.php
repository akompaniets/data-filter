<?php


class ProviderFactoryTest extends \Codeception\TestCase\Test
{
   /**
    * @var \UnitTester
    */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testGetProvider() {
        foreach($this->_getProviderProvider() as $item) {
            $provider = \providers\ProviderFactory::getProvider($item[0]);
            $this->assertEquals($item[1], get_class($provider));
        }
        try {
            \providers\ProviderFactory::getProvider('unknown_provider');
            $this->fail('Unknown provider exception should be thrown');
        } catch (Exception $e) {
        }
    }

    private function _getProviderProvider() {
        return [
            [BASEPATH . '/tests/unit/fixtures/data.php', 'providers\\PhpDataProvider'],
            [BASEPATH . '/tests/unit/fixtures/data.json', 'providers\\JsonDataProvider'],
            [BASEPATH . '/tests/unit/fixtures/data.xml', 'providers\\XmlDataProvider']
        ];
    }

}