<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 20.10.14
 * Time: 16:32
 */

namespace controllers;


use core\Controller;
use models;
use providers\Provider;

class MainController extends Controller{
    function actionIndex() {
        $this->render('index');
    }

    function actionXml() {
        $source = BASEPATH . DIRECTORY_SEPARATOR . 'providers' . DIRECTORY_SEPARATOR . 'test_data' .DIRECTORY_SEPARATOR . 'data.xml';
        $filters = [];
        if (isset($_POST['filter'])) {
            $filters = $_POST['filter'];
        }
        $this->render('data', $this->getData($source, $filters, 'xml'));
    }

    function actionJson() {
        $source = BASEPATH . DIRECTORY_SEPARATOR . 'providers' . DIRECTORY_SEPARATOR . 'test_data' .DIRECTORY_SEPARATOR . 'data.json';
        $filters = [];
        if (isset($_POST['filter'])) {
            $filters = $_POST['filter'];
        }
        $this->render('data', $this->getData($source, $filters, 'json'));
    }

    function actionPhp() {
        $source = BASEPATH . DIRECTORY_SEPARATOR . 'providers' . DIRECTORY_SEPARATOR . 'test_data' .DIRECTORY_SEPARATOR . 'data.php';
        $filters = [];
        if (isset($_POST['filter'])) {
            $filters = $_POST['filter'];
        }
        $this->render('data', $this->getData($source, $filters, 'php'));
    }

    private function getData($source, $filters, $dataType) {
        $model = new models\DataModel();
        $builder = $model->getData($source, $filters);
        return [
            'locales' => $builder->getDataProvider()->getColumn(Provider::LOCALE_INDEX),
            'result' => $builder->decorateFilters()->applyFilter(),
            'builder' => $builder,
            'dataType' => $dataType
        ];
    }
} 